#!/usr/bin/env python2
import socket


# Set IP and port we are connecting to
RHOST = '192.168.52.139'
RPORT = 31337


# Set the buffer to send to the client
BUFF = "Constan\n"


def main():
    # Creating a TCP connection (socket)
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.connect((RHOST, RPORT))
    # Send the buffer to the socket
    s.send(BUFF)
    # Print out what we sent
    print('Sent: {0}'.format(BUFF))
    # Receive some data from the socket
    data = s.recv(1024)
    # Print out what we received
    print('Received: {0}'.format(data))


if __name__ == '__main__':
    main()
